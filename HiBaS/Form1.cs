﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace HiBaS
{
    // Делегаты к события CopyFile
    public delegate void Complet(bool ifComplete);
    public delegate void Progress(string message, int procent);

    public partial class Form1 : Form
    {
        // Используемый диск и куда копировать временный файл
        string toDriveLetter = "";
        string SourseTemporyFilePath = Directory.GetCurrentDirectory()+"//TemporyFile";

        // Размер блока, с какой нумерации идти и на какой заканчивать
        int sizeClaster = 0;
        int FinishSectors = 3;
        int StartSectors = 1;

        // Работа с временем
        int TimeCreatingFile = 0;
        TimeSpan copyTimerValue;
        int[] FileTime;

        // Буфер для Лог файла
        StreamWriter LogWrite;
        
        // Фишки к CopyFile
        public int BufferLenght { get; set; }
        public event Complet OnComplete;
        public event Progress OnProgress;

// ----------------------------------------------------------------------------------//
        private void Start_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            toDriveLetter = driveList.Text;
            sizeClaster = textSizeClaster2value(comboBox2.Text);

            if (driveList.Text == "")
            {
                Log("Не выбрано устройство");
                return;
            }
            if (sizeClaster == 0)
            {
                Log("Не выбран размер кластера");
                return;
            }
            
            FinishSectors = (int)endSector.Value;
            StartSectors = (int)startSector.Value;
            Log("Сектора " + FinishSectors.ToString()+" - "+StartSectors.ToString());
            Log("Размер кластера: " + sizeClaster.ToString() + " Байт");

            Log("Создание временного файла ..... ");
            CreateFile(SourseTemporyFilePath, sizeClaster);
            LogEditLast("готово");

            BufferLenght = 1024;
            FileTime = new int[FinishSectors + 1];

            for (int i = (int)startSector.Value; i <= FinishSectors; i++)
            {
                Application.DoEvents();
                string currentFileName = "sector" + (i).ToString();
                StartCopyTimer();

                Log("Копирование файла " + currentFileName + ".....");
                if (!Directory.Exists(toDriveLetter + "\\HiBaS"))
                    Directory.CreateDirectory(toDriveLetter + "\\HiBaS");

                string DestPath = toDriveLetter + "\\HiBaS\\" + currentFileName;
                CopyFile(SourseTemporyFilePath, DestPath);
                Application.DoEvents();

                int TimeOnProcessing = StopCopyTimer();
                FileTime[i] = TimeOnProcessing;
                if (TimeOnProcessing == 0) LogEditLast("меньше секунды");
                    else LogEditLast(TimeOnProcessing + " сек");

            }
            Log("Завершено");
            Log("------------   \\ m /   --------------");
            Log("Для продолжения выставите интервал в секундах");
            Log("и нажмите \"Начать очистку\" ");

            StartClear.Enabled = true;
        }
       
        /// <summary>
        ///  преобразование из текстовых сокращений в байты
        /// </summary>
        public int textSizeClaster2value(string text)
        {
            if ((text == null) || (text == "")) return 0;
            var splitText = text.Split(' ');
            int multiplier;
            switch (splitText[1].Trim())
            {
                case "Кб":
                    multiplier = 1024;
                    break;
                case "Мб":
                    multiplier = 1024 * 1024;
                    break;
                case "Гб":
                    multiplier = 1024 * 1024 * 1024;
                    break;
                default:
                    multiplier = 1;
                    break;
            }
            return Convert.ToInt32(splitText[0]) * multiplier;
        }

        /// <summary>
        /// Создание файла определенного размера
        /// </summary>
        /// <param name="CreatingPath"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public bool CreateFile(string CreatingPath, int size)
        {
            if (size == 0) return false;
            if (File.Exists(CreatingPath))
                File.Delete(CreatingPath);
            Application.DoEvents();
            FileStream fs = File.Create(CreatingPath, size);
            for (int i = 0; i < sizeClaster; i++)
            {
                fs.WriteByte(2);
            }
            fs.Flush();
            fs.Close();

            return File.Exists(CreatingPath);
        }

        /// <summary>
        /// Возвращаем количество секторов из буквы диска
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public int GetSizeDriver(string driver)
        {
            if (driver=="") return 0;
            if (sizeClaster == 0) sizeClaster = 1024;
            string label = (driver.Trim())[0].ToString();
            DriveInfo di = new DriveInfo(label);
            return Convert.ToInt32(di.TotalFreeSpace / (sizeClaster));
        }
        
        /// <summary>
        /// Старт Секундамера
        /// </summary>
        public void StartCopyTimer()
        {
            copyTimerValue = DateTime.Now.TimeOfDay;
        }

        /// <summary>
        /// Стоп секундомера и возврат количества секунд прошедших с запуска таймера
        /// </summary>
        /// <returns></returns>
        public int StopCopyTimer()
        {
            return Convert.ToInt32((DateTime.Now.TimeOfDay - copyTimerValue).TotalSeconds);
        }

        /// <summary>
        /// Удаление файлов с хороших секторов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartClear_Click(object sender, EventArgs e)
        {
            Log("Начата процедура очистки");
            int max = (int)maxTime.Value;
            for (int i = (int)startSector.Value; i <= FinishSectors; i++)
            {
                if (FileTime[i] < max) continue;
                string currentFileName = "sector" + (i).ToString();
                Log("Удаляю " + currentFileName);
                string delPath = toDriveLetter + "//" + currentFileName;
                try
                {
                    File.Delete(delPath);
                }
                catch (Exception ex)
                {
                    LogEditLast("...удаление не возможно");
                }
                LogEditLast(".....удалено");
            }
            Log("Все операции завершены");

            StartClear.Enabled = false;
        }

        /// <summary>
        /// Отображение в listBox и запись в лог файл
        /// </summary>
        /// <param name="LogText"></param>
        public void Log(string LogText)
        {
            listBox1.Items.Add(LogText);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
            LogWrite.Write("\n" + LogText);
            LogWrite.Flush();
        }

        /// <summary>
        /// Отображение в listBox и запись в лог файл
        /// </summary>
        /// <param name="LogText"></param>
        public void LogEditLast(string addText)
        {
            int i = listBox1.Items.Count - 1;
            string txt = listBox1.Items[i].ToString();
            listBox1.Items.RemoveAt(i);
            listBox1.Items.Insert(i, txt + addText);
            LogWrite.Write(addText);
            LogWrite.Flush();
        }
        
        /// <summary>
        /// Действия при загрузке программы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // Считываем доступные диски и добовляем в список устройств
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            string DriverLabel;
            foreach (DriveInfo d in allDrives)
            {
                DriverLabel = d.Name;
                driveList.Items.Add(DriverLabel);
            }
            // Открываем файл протоколирования
            FileStream Fs = new FileStream(Directory.GetCurrentDirectory() + "/Log.txt", FileMode.Append);
            LogWrite = new StreamWriter(Fs);
        }

        /// <summary>
        /// Пересчет количество секторов в зависимости от размера кластера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void driveList_SelectedIndexChanged(object sender, EventArgs e)
        {
            sizeClaster = textSizeClaster2value(comboBox2.Text);
            int max = GetSizeDriver(driveList.Text);

            startSector.Maximum = max;
            endSector.Maximum = max;
            endSector.Value = max;
        }

        /// <summary>
        /// Копирование файла
        /// </summary>
        /// <param name="sourceFile">Путь к исходному файлу</param>
        /// <param name="destinationFile">Путь к целевому файлу</param>
        public void CopyFile(string sourceFile, string destinationFile)
        {
            try
            {
                //Создаем буфер по размеру исходного файла
                //В буфер будем записывать информацию из файла
                Byte[] streamBuffer = new Byte[BufferLenght];
                //Общее количество считанных байт
                long totalBytesRead = 0;
                //Количество считываний
                //Используется для задания периода отправки сообщений
                int numReads = 0;

                //Готовим поток для исходного файла
                using (FileStream sourceStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
                {
                    //Получаем длину исходного файла
                    long sLenght = sourceStream.Length;
                    //Готовим поток для целевого файла
                    using (FileStream destinationStream = new FileStream(destinationFile, FileMode.Create, FileAccess.Write))
                    {
                        //Читаем из буфера и записываем в целевой файл
                        while (true) //Из цикла выйдем по окончанию копирования файла
                        {
                            //Увеличиваем на единицу количество считываний
                            numReads++;
                            //Записываем в буфер streamBuffer BufferLenght байт
                            //bytesRead содержит количество записанных байт
                            //это количество не может быть больше заданного BufferLenght
                            int bytesRead = sourceStream.Read(streamBuffer, 0, BufferLenght);

                            //Если ничего не было считано
                            if (bytesRead == 0)
                            {
                                //Записываем информацию о процессе
                                getInfo(sLenght, sLenght);
                                //и выходим из цикла
                                break;
                            }

                            //Записываем данные буфера streamBuffer в целевой файл
                            destinationStream.Write(streamBuffer, 0, bytesRead);
                            //Для статистики запоминаем сколько уже байт записали
                            totalBytesRead += bytesRead;

                            //Если количество считываний кратно 10
                            if (numReads % 10 == 0)
                            {
                                //Записываем информацию о процессе
                                getInfo(totalBytesRead, sLenght);
                            }

                            //Если количество считанных байт меньше буфера
                            //Значит это конец
                            if (bytesRead < BufferLenght)
                            {
                                //Записываем информацию о процессе
                                getInfo(totalBytesRead, sLenght);
                                break;
                            }
                        }
                    }
                }

                //Отправляем сообщение что процесс копирования закончен удачно
                if (OnComplete != null)
                    OnComplete(true);
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Возникла следующая ошибка при копировании:\n" + e.Message);
                //Отправляем сообщение что процесс копирования закончен неудачно
                System.Threading.Thread.Sleep(10000);
                if (OnComplete != null)
                    OnComplete(false);

            }
        }

        private void getInfo(long totalBytesRead, long sLenght)
        {
            //Формируем сообщение
            string message = string.Empty;
            double pctDone = (double)((double)totalBytesRead / (double)sLenght);
            message = string.Format("Считано: {0} из {1}. Всего {2}%",
                     totalBytesRead,
                     sLenght,
                     (int)(pctDone * 100));
            //Отправляем сообщение подписавшимя на него
            if (OnProgress != null && !double.IsNaN(pctDone)) //Изменение 1.03.2012
                OnProgress(message, (int)(pctDone * 100));

        }

        public Form1()
        {
            InitializeComponent();
        }


    }
}
