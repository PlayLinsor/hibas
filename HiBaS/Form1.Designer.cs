﻿namespace HiBaS
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.driveList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.Start = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.maxTime = new System.Windows.Forms.NumericUpDown();
            this.StartClear = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.startSector = new System.Windows.Forms.NumericUpDown();
            this.endSector = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.maxTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startSector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endSector)).BeginInit();
            this.SuspendLayout();
            // 
            // driveList
            // 
            this.driveList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.driveList.FormattingEnabled = true;
            this.driveList.Location = new System.Drawing.Point(10, 58);
            this.driveList.Name = "driveList";
            this.driveList.Size = new System.Drawing.Size(262, 21);
            this.driveList.TabIndex = 1;
            this.driveList.SelectedIndexChanged += new System.EventHandler(this.driveList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Courier New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hidden Bad-Sector";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(-1, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(274, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Выберите устройство";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Размер кластера";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Выбирите диск и нажмите кнопку старта"});
            this.listBox1.Location = new System.Drawing.Point(9, 256);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(261, 199);
            this.listBox1.TabIndex = 6;
            // 
            // comboBox2
            // 
            this.comboBox2.DisplayMember = "1";
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.comboBox2.Items.AddRange(new object[] {
            "1 Кб",
            "16 Кб",
            "256 Кб",
            "1 Мб",
            "16 Мб",
            "256 Мб",
            "1 Гб"});
            this.comboBox2.Location = new System.Drawing.Point(171, 97);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(101, 21);
            this.comboBox2.TabIndex = 7;
            this.comboBox2.ValueMember = "1";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.driveList_SelectedIndexChanged);
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(2, 461);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(119, 23);
            this.Start.TabIndex = 8;
            this.Start.Text = "Начать заполнение";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Макс.  граница(сек)";
            // 
            // maxTime
            // 
            this.maxTime.Location = new System.Drawing.Point(171, 132);
            this.maxTime.Name = "maxTime";
            this.maxTime.Size = new System.Drawing.Size(99, 20);
            this.maxTime.TabIndex = 10;
            this.maxTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // StartClear
            // 
            this.StartClear.Enabled = false;
            this.StartClear.Location = new System.Drawing.Point(159, 461);
            this.StartClear.Name = "StartClear";
            this.StartClear.Size = new System.Drawing.Size(111, 23);
            this.StartClear.TabIndex = 11;
            this.StartClear.Text = "Начать очистку";
            this.StartClear.UseVisualStyleBackColor = true;
            this.StartClear.Click += new System.EventHandler(this.StartClear_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Сектора";
            // 
            // startSector
            // 
            this.startSector.Location = new System.Drawing.Point(116, 166);
            this.startSector.Name = "startSector";
            this.startSector.Size = new System.Drawing.Size(65, 20);
            this.startSector.TabIndex = 13;
            this.startSector.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // endSector
            // 
            this.endSector.Location = new System.Drawing.Point(205, 166);
            this.endSector.Name = "endSector";
            this.endSector.Size = new System.Drawing.Size(65, 20);
            this.endSector.TabIndex = 14;
            this.endSector.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(187, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 496);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.endSector);
            this.Controls.Add(this.startSector);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.StartClear);
            this.Controls.Add(this.maxTime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.driveList);
            this.Name = "Form1";
            this.Text = "HiBaS Alpha 0.0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.maxTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startSector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endSector)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox driveList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown maxTime;
        private System.Windows.Forms.Button StartClear;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown startSector;
        private System.Windows.Forms.NumericUpDown endSector;
        private System.Windows.Forms.Label label6;
    }
}

